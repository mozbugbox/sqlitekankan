#!/usr/bin/python
# vim:fileencoding=utf-8:sw=4:et

from __future__ import print_function, unicode_literals, absolute_import
import sys
import os

class SqliteDB:
    """A class to hold the database related actions"""
    def __init__(self, fname):
        """@fname: a sqlite database file."""
        self.fname = fname
        import sqlite3
        self.db = sqlite3.connect(fname)
        self.cur = self.db.cursor()

    def __del__(self):
        self.db.close()

    def get_master(self):
        """Return sqlite_master table content."""
        stmt = "SELECT * FROM sqlite_master;"
        return self.query_list(stmt)

    def get_table_info(self, table_name):
        """Return a db table info."""
        stmt = "PRAGMA table_info( {} );".format(table_name)
        return self.query_list(stmt)

    def query(self, stmt, *args):
        """Execute a query and return the cursor."""
        self.cur.execute(stmt, *args)
        return self.cur

    def query_list(self, stmt, *args):
        """Execute a sql query and return the result as a list."""
        return list(self.query(stmt, *args))

    def query_all_with_range(self, table_name, limit, offset, sortby=None):
        """Return the content of a table with limit number of results."""
        if sortby is None:
            stmt = "SELECT * FROM {} LIMIT ? OFFSET ?;".format(table_name)
            return self.query(stmt, (limit, offset))
        else:
            if isinstance(sortby, unicode):
                sortby = [sortby]
            sort_num = len(sortby)
            sorttxt = ", ".join(["?"]* sort_num)
            stmt = "SELECT * FROM {} ORDER BY {} LIMIT ? OFFSET ?;".format(
                        table_name, sorttxt)
            param = sortby[:]
            param.append(limit)
            param.append(offset)
            return self.query(stmt, param)

    def index_info_as_string(self, table_name):
        """Convert db index info to text."""
        cur = self.query("PRAGMA index_info( {} )".format(table_name))
        header = "{:10s}{:10s}{:10s}".format("index", "table", "name")
        msg = [header]
        for l in cur:
            ltxt = [str(x) for x in l]
            msg.append("{:10s}{:10s}{:10s}".format(*ltxt))
        return "\n".join(msg)


def main():
    pass

if __name__ == '__main__':
    main()

