#!/usr/bin/python
#vim:fileencoding=utf-8:sw=4
# Sqlite Database File Viewer
# Copyright mozbugbox 2012
# License: GPL v3 or later.

from __future__ import print_function, absolute_import
#from __future__ import unicode_literals
__version__ = "0.1"

import sys
import os
import logging as log
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GObject
from gi.repository import Pango

import re

DATA_DIR=None
UI_FNAME='ui/sqlitekankan.ui'
UI_FULL_PATH=""

BLOB_SIZE = 12
RESPONSE_CANCEL=1
RESPONSE_OPEN=2

def get_next_iter(model, citer):
    '''Get the next iter on the model.'''
    ret = None
    if model.iter_has_child(citer):
        ret = model.iter_children(citer)
    else:
        niter = model.iter_next(citer)
        if niter is None:
            niter = model.iter_parent(citer)
            if niter is None:
                niter = model.get_iter_first()
            else:
                niter = model.iter_next(niter)
        ret = niter
    return ret

class TableView(GObject.GObject):
    """A grid which hold a view over a given database table."""
    page_max = 1000
    def __init__(self, db, table_name):
        """@db: a sqlite database
           @table_name: a table name in string
           @grid: a Gtk.Grid to hold related widgets
        """
        self.db = db
        self.table_name = table_name
        self.grid = None
        self.setup_ui()

    def setup_treeview(self, tree):
        """setup the treeview for the db table contents."""
        info = self.db.get_table_info(self.table_name)
        model = tree.get_model()
        # a list of [name, type] for columns
        infolist = [x[1:3] for x in info]
        types = []
        sqlite_types = {"INTEGER", "REAL", "TEXT"}
        for i in range(len(infolist)):
            pair = infolist[i]
            renderer = Gtk.CellRendererText()
            title_label = Gtk.Label(pair[0])
            title_label.show()
            col = Gtk.TreeViewColumn(None, renderer, text=i)
            col.set_widget(title_label)
            col.props.resizable = True
            tree.append_column(col)

            """
            if pair[1] == "INTEGER":
                types.append(int)
            elif pair[1] == "REAL":
                types.append(float)
            elif pair[1] == "TEXT":
                types.append(str)
            """
            types.append(str)
            if pair[1] not in sqlite_types:
                renderer.props.ellipsize = Pango.EllipsizeMode.END
                renderer.props.width_chars = BLOB_SIZE

        model = Gtk.ListStore(*types)
        tree.set_model(model)

    def setup_combobox(self, combobox):
        """Setup the combobox which shows next batch of result."""
        cur = self.db.query("SELECT COUNT(*) FROM {};".format(self.table_name))
        for row in cur:
            count = row[0]
            break

        for i in range(0, count, self.page_max):
            lower = i
            upper = min(i+self.page_max, count)
            text = "{} - {}".format(lower+1, upper)
            combobox.append_text(text)
            combobox.set_active(0)
        if count == 0:
            combobox.append_text("Empty")
            combobox.set_active(0)

    def on_combobox_changed(self, combobox, *args):
        """on combobox changed signal."""
        text = combobox.get_active_text()
        if text is None or text == "Empty":
            return
        lower, s, upper = text.partition("-")
        lower = int(lower.strip()) - 1
        upper = int(upper.strip())
        #print(upper, lower)
        self.fill_table_tree(lower, upper)

    def on_arrow_right(self, widget, evt, combobox):
        """on arrow right button released signal."""
        num = combobox.get_active()
        model = combobox.get_model()
        num += 1
        if 0 > num or num >= len(model):
            num = 0
        combobox.set_active(num)

    def on_arrow_left(self, widget, evt, combobox):
        """on arrow left button released signal."""
        num = combobox.get_active()
        model = combobox.get_model()
        num -= 1
        if 0 > num or num >= len(model):
            num = len(model) - 1
        combobox.set_active(num)

    def setup_ui(self):
        """Setup ui for the class."""
        grid = Gtk.Grid()
        tree = Gtk.TreeView()
        tree.props.hexpand = True
        tree.props.vexpand = True
        tree.props.headers_visible = True
        self.setup_treeview(tree)

        sw = Gtk.ScrolledWindow()
        sw.add(tree)
        grid.attach(sw, 0, 0, 1, 1)
        builder = Gtk.Builder()
        builder.add_objects_from_file(UI_FULL_PATH, ["grid_combobox_arrow"])
        grid_combobox_arrow = builder.get_object("grid_combobox_arrow")
        grid.attach(grid_combobox_arrow, 0, 1, 1, 1)

        combobox = builder.get_object("comboboxtext_table_limit")
        self.grid_combobox_arrow = grid_combobox_arrow

        self.grid = grid
        self.tree = tree
        self.combobox = combobox


        arrow_left = builder.get_object("eventbox_arrow_left")
        arrow_right = builder.get_object("eventbox_arrow_right")
        arrow_left.connect("button-release-event", self.on_arrow_left, combobox)
        arrow_right.connect("button-release-event", self.on_arrow_right,
                combobox)

        combobox.connect("changed", self.on_combobox_changed)
        self.setup_combobox(combobox)
        grid.show_all()

    def fill_table_tree(self, lower, upper):
        """Fill the treeview for the db table with limited num of results."""
        data = self.db.query_all_with_range(self.table_name,
                upper - lower, lower)
        tree = self.tree
        model = tree.get_model()
        model.clear()
        def to_unicode(tmp):
            """Hope we can handle it."""
            if isinstance(tmp, buffer):
                tmp = unicode(str(tmp[:BLOB_SIZE+8]), "string_escape")
            elif not isinstance(tmp, (str, unicode)):
                tmp = unicode(tmp)
            return tmp

        def _fill_it():
            n = 0
            step = 300
            tree.freeze_child_notify()
            for row in data:
                try:
                    model.append([to_unicode(x) for x in row])
                except ValueError:
                    log.error("{}".format(sys.exc_info()[1]))
                    log.error(row)
                n+=1
                if n % step == 0:
                    tree.thaw_child_notify()
                    yield True
                    tree.freeze_child_notify()
            tree.thaw_child_notify()

        fill_action = _fill_it()
        def runit():
            try:
                fill_action.next()
                return True
            except StopIteration:
                return False
        GObject.idle_add(runit)

    def refresh(self):
        """Refresh table contents."""
        model = self.combobox.get_model()
        model.clear()
        self.setup_combobox(self.combobox)

ui_info = \
"""<ui>
    <menubar name="MenuBar">
        <menu action="FileMenu">
            <menuitem action="Open"/>
            <menuitem action="CloseTable"/>
            <menuitem action="RefreshTable"/>
            <separator/>
            <menuitem action="Quit"/>
        </menu>
        <menu action="ViewMenu">
            <menuitem action="Find"/>
            <menuitem action="FindNext"/>
        </menu>
        <menu action="HelpMenu">
            <menuitem action="HelpContent"/>
            <menuitem action="About"/>
        </menu>
    </menubar>
    <toolbar name="ToolBar">
        <toolitem action="Quit"/>
        <toolitem action="Open"/>
        <toolitem action="CloseTable"/>
        <toolitem action="RefreshTable"/>
        <separator action="Sep1"/>
        <placeholder name="FindEntry"/>
        <toolitem action="Find"/>
    </toolbar>
</ui>"""

class App:
    '''Application control class.'''
    def __init__(self):
        self.db = None
        self.table_map = {} # map table name to TableView

        self.textview_master = None
        self.search_entry = None
        self.builder = None # gtk Builder
        self.load_gui()

    def load_gui(self):
        '''Load gui, what else?'''
        # name, stock_id, label, accelerator, tooltip, callback
        action_entries = [
            ["FileMenu", None, "_File"],
            ["Open", Gtk.STOCK_OPEN, None, None, None, self.on_open_action],
            ["Quit", Gtk.STOCK_QUIT, None, None, None, self.on_quit_action],
            ["ViewMenu", None, "_View"],
            ["CloseTable", Gtk.STOCK_CLOSE, None, None, "Close current table",
                self.on_close_table_action],
            ["RefreshTable", Gtk.STOCK_REFRESH, None, None,
                "Refresh current Table", self.on_refresh_table_action],
            ["Find", Gtk.STOCK_FIND],
            ["FindNext", None, "_Find Next"],
            ["HelpMenu", None, "_Help"],
            ["HelpContent", Gtk.STOCK_HELP, None, None, None,
                self.on_help_action],
            ["About", Gtk.STOCK_ABOUT, None, None, None, self.on_about_action],
        ]

        builder = Gtk.Builder()
        builder.add_from_file(UI_FULL_PATH)
        self.builder = builder

        win = builder.get_object('window_main')
        win.resize(750, 500)

        # setup main menu through UIManager.
        actions = Gtk.ActionGroup("main_actions")
        actions.add_actions(action_entries)
        ui = Gtk.UIManager()
        ui.insert_action_group(actions, 0)
        win.add_accel_group(ui.get_accel_group())
        mergeid = ui.add_ui_from_string(ui_info)
        grid = builder.get_object('grid_main')

        menu = ui.get_widget("/MenuBar")
        grid.attach(menu, 0, 0, 1, 1)

        tbar = ui.get_widget("/ToolBar")
        grid.attach(tbar, 0, 1, 1, 1)

        tool_item = Gtk.ToolItem()
        search_entry = Gtk.Entry()
        self.search_entry = search_entry
        tool_item.add(search_entry)
        find_entry = ui.get_widget("/ToolBar/FindEntry")
        find_entry_index = tbar.get_item_index(find_entry)
        tbar.insert(tool_item, find_entry_index)
        tbar.remove(find_entry)

        # FIXME: Don't really work well.
        openbutton = ui.get_widget("/ToolBar/Open")
        notebook = self.builder.get_object("notebook_main")
        tree_master = self.builder.get_object("treeview_master")
        focus_chain = [openbutton, notebook, tree_master]
        # FIXME: toolbar don't handle focus chain?
        grid.set_focus_chain(focus_chain)

        win.connect('delete-event', self.quit)
        win.show_all()

        # delay setup_ui for faster startup
        GObject.idle_add(self.setup_ui)

    def setup_ui(self):
        """Setup the GUI defaults."""

        tree_master = self.builder.get_object("treeview_master")
        tree_master.connect("cursor-changed",
                self.on_treeview_master_cursor_changed)
        tree_master.connect("row-activated",
                self.on_treeview_master_row_activated)

        self.textview_master = self.builder.get_object("textview_master")
        self.setup_gtksourceview()
        self.textview_master.modify_font(Pango.FontDescription("monospace"))

        open_dialog = self.builder.get_object('filechooserdialog_main_open')
        open_dialog.connect("response", self.on_open_dialog_response)
        open_dialog.connect("close", self.gtk_widget_hide)
        about_dialog = self.builder.get_object("aboutdialog_main")
        about_dialog.connect("close", self.gtk_widget_hide)
        about_dialog.connect("response", self.gtk_widget_hide)
        help_win = self.builder.get_object("window_help")
        help_win.connect("key-release-event", self.on_window_escape_hide)
        help_win.connect("delete-event", self.gtk_widget_hide)
        return

    def setup_gtksourceview(self):
        '''Replace textview with gtksourceview.'''
        try:
            from gi.repository import GtkSource
        except ImportError:
            #print('Failed to import GtkSource!')
            return
        lm = GtkSource.LanguageManager()
        textview = GtkSource.View()
        buf = GtkSource.Buffer()
        textview.set_buffer(buf)
        lang = lm.guess_language(None, 'text/sql')
        lang = lm.get_language('sql')

        textview.set_wrap_mode(Gtk.WrapMode.WORD)
        textview.set_editable(False)
        buf.set_language(lang)
        buf.set_highlight_syntax(True)
        self.textview_master = textview

        sw = self.builder.get_object('scrolledwindow_textview_master')
        kid = sw.get_child()
        sw.remove(kid)
        sw.add_with_viewport(textview)
        sw.show_all()

    def on_treeview_master_cursor_changed(self, tree, *args):
        if self.db is None: return
        model = tree.get_model()
        path = tree.get_cursor()[0]
        miter = model.get_iter(path)
        ttype, table_name, data = model.get(miter, 0, 1, 4)
        if data is None:
            if ttype == "index":
                data = self.db.index_info_as_string(table_name)
                model.set_value(miter, 4, data)
            else:
                data = ""
        tview = self.textview_master
        tbuf = tview.get_buffer()
        tbuf.set_text(data)

    def on_treeview_master_row_activated(self, tree, path, col, *args):
        model = tree.get_model()
        miter = model.get_iter(path)
        ttype, name = model.get(miter, 0, 2)
        if ttype != "table":
            return

        notebook = self.builder.get_object("notebook_main")
        if name not in self.table_map:
            table_view = TableView(self.db, name)
            label = Gtk.Label(name)
            notebook.append_page(table_view.grid, label)

            label.props.ellipsize = Pango.EllipsizeMode.MIDDLE
            label.props.tooltip_text = name
            label.show()
            label.props.xalign = 0.0

            self.table_map[name] = table_view

        kid = self.table_map[name].grid
        num = notebook.page_num(kid)
        if num > 0:
            notebook.set_current_page(num)

    def on_open_action(self, *args):
        '''Load file.'''
        dialog = self.builder.get_object('filechooserdialog_main_open')
        dialog.show()

    def on_quit_action(self, *args):
        '''Quit action.'''
        self.quit()

    def on_close_table_action(self, *args):
        """close current table under the given tab"""
        notebook = self.builder.get_object("notebook_main")
        num = notebook.get_current_page()
        page = notebook.get_nth_page(num)
        text = notebook.get_tab_label_text(page)
        if text in self.table_map:
            del self.table_map[text]
            notebook.remove_page(num)
        elif text == "sqlite_master":
            self.db = None
            self.reset_db()

    def on_refresh_table_action(self, *args):
        """Refresh table content under current tab"""
        notebook = self.builder.get_object("notebook_main")
        num = notebook.get_current_page()
        page = notebook.get_nth_page(num)
        text = notebook.get_tab_label_text(page)
        if text in self.table_map:
            stock_view = self.table_map[text]
            stock_view.refresh()
        elif text == "sqlite_master":
            self.reset_db()

    def set_db_file(self, fname):
        """Set current database file."""
        import sqldb
        db = sqldb.SqliteDB(fname)
        self.db = db
        self.reset_db()

    def reset_db(self):
        """Reset GUI for the new database."""
        self.table_map = {}
        notebook = self.builder.get_object("notebook_main")
        n = notebook.get_n_pages()

        tab_index = n - 1
        while tab_index >= 0:
            kid = notebook.get_nth_page(tab_index)
            if kid is not None:
                text = notebook.get_tab_label_text(kid)
                if text != "sqlite_master":
                    notebook.remove_page(tab_index)
            tab_index -= 1

        tree_master = self.builder.get_object("treeview_master")
        model_master = tree_master.get_model()
        model_master.clear()
        if self.db is not None:
            db_info = self.db.get_master()
            for row in db_info:
                model_master.append(row)
        tview_master = self.textview_master
        tbuf = tview_master.get_buffer()
        tbuf.set_text("")

    def on_open_dialog_response(self, win, resp, *args):
        '''Open file dialog.'''
        if resp == RESPONSE_OPEN:
            fname = win.get_filename()
            if os.path.isdir(fname) == True:
                win.set_current_folder(fname)
                return
            self.set_db_file(fname)
        win.hide()
        return True

    def on_about_action(self, *args):
        '''Show the About dialog.'''
        dl = self.builder.get_object("aboutdialog_main")
        dl.show()

    def on_help_action(self, *args):
        '''Show the Help window.'''
        hw = self.builder.get_object("window_help")
        hw.show_all()
        hw.present()

    def on_window_escape_hide(self, wid, evt, *args):
        '''Hide window on escape key.'''
        if evt.keyval == Gdk.KEY_Escape and evt.state == 0:
            wid.hide()
            return True

    def gtk_widget_hide(self, wid, *args):
        '''Generic funciton to hide a given window widget.'''
        wid.hide()
        return True

    def run(self):
        '''Start point.'''
        Gtk.main()

    def quit(self, *args):
        '''General quit function.'''
        Gtk.main_quit()
        return True

def parse_args():
    import argparse
    parser = argparse.ArgumentParser(
            description="Download folder content from GitHub.")
    parser.add_argument("filename", nargs="?", help="Database file name")
    parser.add_argument("-D", "--debug", action="store_true",
            help="Debug run")
    parser.add_argument("--version", action="version",
            version="%(prog)s {}".format(__version__))
    args, rest = parser.parse_known_args()
    return args

def main():
    global DATA_DIR, UI_FULL_PATH

    args = parse_args()

    log_level = log.INFO
    if args.debug:
        log_level = log.DEBUG
    log.basicConfig(format="%(levelname)s>> %(message)s", level=log_level)

    # If UI_FULL_PATH is not set, try to set it again.
    data_dir = DATA_DIR
    if data_dir is None:
        try:
            import site_sqlitekankan
            data_dir = site_sqlitekankan.data_dir
        except ImportError:
            dirname = os.path.dirname(os.path.abspath(__file__))
            basename = os.path.dirname(sys.argv[0])
            if dirname.endswith("/src"):
                basename = 'src/../ui/..'
            import util
            data_dir = util.search_data_dir(basename)
        DATA_DIR = data_dir
    if data_dir is not None:
        UI_FULL_PATH = os.path.join(data_dir, UI_FNAME)
    else:
        print("Cannot open data file {} ...".format(UI_FNAME))
        sys.exit(2)

    from gi.repository import GLib
    GLib.set_application_name("sqlitekankan")
    app = App()
    if args.filename is not None:
        fname = args.filename
        GObject.idle_add(app.set_db_file, fname)
    app.run()

if __name__ == '__main__':
    main()
