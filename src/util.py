#!/usr/bin/python
#vim:fileencoding=utf-8:sw=4:et

import sys
import os

def search_data_dir(data_basename):
    '''Search the data_files directory on the installed system
    given a data_files basename.

    We try to find the data_basename pattern in the parents of
    our source code (__file__).

    @data_basename: a partial path of the target data directory.
    '''
    cwd = os.path.abspath(os.path.dirname(__file__))

    ddir = None
    while True:
        ret= os.path.exists(os.path.join(cwd, data_basename))
        if ret is True:
            break
        else:
            if cwd in set(['/', '']):
                cwd = None
                break
            cwd = os.path.abspath(os.path.join(cwd, '..'))
    if cwd is not None:
        ddir = os.path.join(cwd, data_basename)
    return ddir

def main():
    print search_data_dir('ui')
    print search_data_dir('adifasdfo')

if __name__ == '__main__':
    main()

